# Assignment 3 
## Luca Gandolfi 807485

The aim of this project is to represent and persist 4 entities that are in relationship each others:
- Team
- Team Member
- Project
- Client

To run this project you need to import the objectdb.jar file in the eclipse project folder, like a library.
The objectdb jar file can be downloaded [here](https://www.objectdb.com/download), version 2.7.6_01.
In addition you have to use EclipseLink 2.5.2 and the JRE System Library (JavaSE-1.8)

The relationships are:
- a relation one-to-many between a Team and its Team Member
- a relation one-to-many between a Team and the Projects that the Team works on
- a relation one-to-many between a Client and the Projects required by that Client
- a self-relation on the Team Member entity, about Team Manager and normal Team Member

## Tests and Uses
There is a package "test" which contains tests that cover over 98% of the packages "assignment3" and "services", containing the source code. The percentage is given by [EclEmma](https://www.eclemma.org/) tool, but you know that tests are never exhaustive.

JUnit Jupiter was used to create test cases.

To run tests you don't have to follow an order, just right click on the test package and select _run with junit_. The db is auto emptied and populated when needed.

To use rightly this program, an User should only use classes named as _Service_. The class Operations still contains a _drop table_ method useful only for tests.

## Run in Eclipse
1. Make sure you have downloaded the objectdb package at the link in the first section.
2. Start a new **JPA Project** on Eclipse 2018-09 (4.9.0).
3. Include the EclipseLink 2.5.2 library during the project creation steps and download it using the wizard if you don't have it already.
4. Import the objectdb.jar file in a library folder.
5. You can now import the Java classes and update the xml file adding the entity names.
6. The path for the database is a Linux path, so you should modify it on Windows and you could not change it in Linux. The database will be created in the objectdb folder if you keep the structure.
7. The project is now set up rightly.

WHERE CHANGE THE PATH?

If you have to change the path, you can find it:
- assignment3 package, JPADao class, connect() method
- services package, Operations class, connect() method
- services package, QueryExecutor class, connect() method
