package test;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Calendar;
import java.util.List;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import assignment3.Client;
import assignment3.JPADao;
import assignment3.Project;
import assignment3.Team;
import assignment3.Team_Member;
import services.Operations;
import test.Creates;

class FUNCTIONALTest {
	
	@BeforeEach
	public void beforeMethods() {
		Creates creates = new Creates();
		creates.createClients();
		creates.createTeams();
		creates.createProjects();
		creates.createTeam_Member();
	}
	
	@AfterEach
	public void afterMethods() {
		Operations<Team> oTeam = new Operations<Team>(Team.class);
		Operations<Client> oClient = new Operations<Client>(Client.class);
		Operations<Project> oProj = new Operations<Project>(Project.class);
		Operations<Team_Member> oTM = new Operations<Team_Member>(Team_Member.class);
		
		int result;
		result = oTeam.drop();
		result = oClient.drop();
		result = oProj.drop();
		result = oTM.drop();
	}

	@Test
	public void testEqualsTeam() {
		//Creates creates = new Creates();
		JPADao<Long, Team> dao;
		dao = new JPADao<Long, Team>(Team.class);
		Operations<Team> o = new Operations<Team>(Team.class);
		
		List<Long> ids = o.getIDs();
		
		long id = ids.get(0);
		Team t1 = dao.read(id);
		//leggo t2 uguale a t1
		Team t2 = dao.read(id);
		id++;
		//t3 diverso da t1 e t2
		Team t3 = dao.read(id);
		
		//ora testo le uguaglianze
		assertTrue(t1.equals(t2));
		assertTrue(!t1.equals(t3));
		
		//o.drop();
	}
	
	@Test
	public void testEqualsCLient() {
		JPADao<Long, Client> dao;
		dao = new JPADao<Long, Client>(Client.class);
		Operations<Client> o = new Operations<Client>(Client.class);
		
		List<Long> ids = o.getIDs();
		
		long id = ids.get(0);
		Client c1 = dao.read(id);
		//leggo c2 uguale a c1
		Client c2 = dao.read(id);
		id++;
		//c3 diverso da c1 e c2
		Client c3 = dao.read(id);
		
		//ora testo le uguaglianze
		assertTrue(c1.equals(c2));
		assertTrue(!c1.equals(c3));
	}
	
	
	@Test
	public void testEqualsTeam_Member() {
		JPADao<Long, Team_Member> dao;
		dao = new JPADao<Long, Team_Member>(Team_Member.class);
		Operations<Team_Member> o = new Operations<Team_Member>(Team_Member.class);
		
		List<Long> ids = o.getIDs();
		long id = ids.get(0);
		Team_Member t1 = dao.read(id);
		//leggo t2 uguale a t1
		Team_Member t2 = dao.read(id);
		id++;
		//t3 diverso da t1 e t2
		Team_Member t3 = dao.read(id);
		
		//ora testo le uguaglianze
		assertTrue(t1.equals(t2));
		assertTrue(!t1.equals(t3));	
	}
	
	@Test
	public void testEqualsProject() {
		JPADao<Long, Project> dao;
		dao = new JPADao<Long, Project>(Project.class);
		Operations<Project> o = new Operations<Project>(Project.class);
		
		List<Long> ids = o.getIDs();
		long id = ids.get(0);
		Project p1 = dao.read(id);
		//leggo p2 uguale a p1
		Project p2 = dao.read(id);
		id++;
		//p3 diverso da p1 e p2
		Project p3 = dao.read(id);
		
		//ora testo le uguaglianze
		assertTrue(p1.equals(p2));
		assertTrue(!p1.equals(p3));			
	}
	
	@Test
	public void testFindAll() {
		JPADao<Long, Client> daoC;
		daoC = new JPADao<Long, Client>(Client.class);
		
		assertTrue(daoC.findAll().size()>0);
		
		JPADao<Long, Team> daoT;
		daoT = new JPADao<Long, Team>(Team.class);
		
		assertTrue(daoT.findAll().size()>0);
		
		JPADao<Long, Team_Member> daoTM;
		daoTM = new JPADao<Long, Team_Member>(Team_Member.class);
		
		assertTrue(daoTM.findAll().size()>0);
		
		JPADao<Long, Project> daoP;
		daoP = new JPADao<Long, Project>(Project.class);
		
		assertTrue(daoP.findAll().size()>0);
	}
	
	@Test
	public void testSetterGetterProject() {
		JPADao<Long, Project> dao;
		dao = new JPADao<Long, Project>(Project.class);
		Operations<Project> o = new Operations<Project>(Project.class);
		
		List<Long> ids = o.getIDs();
		long id = ids.get(0);
		
		Project p = dao.read(id);
		
		//get id
		assertEquals(p.getID(), id);
		
		//salvo la version
		int upt = p.getUPT();

		//set and get name
		p.setName("Project_updated");
		p = dao.update(p);
		assertEquals(p.getName(), "Project_updated");
		//la versione sarà aumentata
		assertTrue(upt < p.getUPT());
		
		//set and get Start
		final Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, -250);
		p.setStart(cal.getTime());
		p = dao.update(p);
		assertEquals(p.getStart(), cal.getTime());
		
		//set and get End
		cal.add(Calendar.DATE, +50);
		p.setEnd(cal.getTime());
		p = dao.update(p);
		assertEquals(p.getEnd(), cal.getTime());
		
		//set and get Team		
		JPADao<Long, Team> daoTeam;
		daoTeam = new JPADao<Long, Team>(Team.class);
		Operations<Team> opTeam = new Operations<Team>(Team.class);
		ids = opTeam.getIDs();
		id = ids.get(2);
		//leggo il team da associare
		Team t = daoTeam.read(id);	
		p.setTeam(t);
		p = dao.update(p);
		//controllo che il nuovo team sia uguale a quello associato
		assertTrue(p.getTeam().equals(t));
		
		//set and get Client
		JPADao<Long, Client> daoC;
		daoC = new JPADao<Long, Client>(Client.class);
		Operations<Client> opClient = new Operations<Client>(Client.class);
		ids = opClient.getIDs();
		id = ids.get(2);
		//leggo il client da associare
		Client c = daoC.read(id);
		p.setClient(c);
		p = dao.update(p);
		//controllo che il nuovo client sia uguale a quello associato
		assertTrue(p.getClient().equals(c));
	}
	
	@Test
	public void testSetterGetterTeam_Member() {
		JPADao<Long, Team_Member> dao;
		dao = new JPADao<Long, Team_Member>(Team_Member.class);
		Operations<Team_Member> o = new Operations<Team_Member>(Team_Member.class);
		
		List<Long> ids = o.getIDs();
		long id = ids.get(0);
		Team_Member tm = dao.read(id);
		
		//get id
		assertEquals(tm.getID(),id);
		
		//salvo la versione
		int version = tm.getUPT();
		
		//set and get Team
		JPADao<Long, Team> daoTeam;
		daoTeam = new JPADao<Long, Team>(Team.class);
		Operations<Team> opTeam = new Operations<Team>(Team.class);
		ids = opTeam.getIDs();
		id = ids.get(2);
		//leggo il team da associare
		Team t = daoTeam.read(id);	
		tm.setTeam(t);
		tm = dao.update(tm);
		assertTrue(tm.getTeam().equals(t));
		//la versione deve essere aumentata
		assertTrue(version < tm.getUPT());
	}
	
	@Test
	public void testSetterGetterClient() {
		JPADao<Long, Client> dao;
		dao = new JPADao<Long, Client>(Client.class);
		Operations<Client> o = new Operations<Client>(Client.class);
		
		List<Long> ids = o.getIDs();
		long id = ids.get(0);
		Client c = dao.read(id);
		
		//get id
		assertEquals(c.getID(),id);
		
		//salvo la versione
		int version = c.getUPT();
		
		//set and get Name
		c.setName("NomeFasullo");
		c = dao.update(c);
		assertEquals(c.getName(),"NomeFasullo");
		
		//la versione deve essere cambiata
		assertTrue(version < c.getUPT());
	}

}
