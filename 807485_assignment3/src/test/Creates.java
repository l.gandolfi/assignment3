package test;

import java.util.Calendar;
import java.util.List;

import assignment3.Client;
import assignment3.JPADao;
import assignment3.Project;
import assignment3.Team;
import assignment3.Team_Member;
import services.Operations;

public class Creates {
	/*
	 * classe che contiene i metodi per le operazioni create
	 */
	
	//funzione da richiamare quando è necessario testare con la presenza di oggetti Team
	public void createTeams() {
		JPADao<Long, Team> dao;
		dao = new JPADao<Long, Team>(Team.class);
			
		final Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, -360);
			
		//1
		Team a = new Team();
		a.setCode(990);
		a.setStart(cal.getTime());
		cal.add(Calendar.DATE, +265);
		a.setEnd(cal.getTime());
		dao.create(a);
		//2
		Team b = new Team();
		b.setCode(991);
		cal.add(Calendar.DATE, -103);
		b.setStart(cal.getTime());
		cal.add(Calendar.DATE, +140);
		b.setEnd(cal.getTime());
		dao.create(b);
		//3
		Team c = new Team();
		c.setCode(992);
		cal.add(Calendar.DATE, -174);
		c.setStart(cal.getTime());
		cal.add(Calendar.DATE, +70);
		c.setEnd(cal.getTime());
		dao.create(c);
	}
		
	//funzione da chiamare quando è necessario creare oggetti Team_Member
	public void createTeam_Member() {
		JPADao<Long, Team> daoTeam;
		daoTeam = new JPADao<Long, Team>(Team.class);
			
		Operations<Team> opTeam = new Operations<Team>(Team.class);
		List<Long> nIDs = opTeam.getIDs(); 
			
		long id = nIDs.get(0);
		Team A = daoTeam.findById(id);
		id++;
		Team B = daoTeam.findById(id);
		id++;
		Team C = daoTeam.findById(id);
		
		JPADao<Long, Team_Member> daoTM;
		daoTM = new JPADao<Long, Team_Member>(Team_Member.class);		
		
		Team_Member t1 = new Team_Member();
		Team_Member t2 = new Team_Member();
		Team_Member t3 = new Team_Member();
		Team_Member t4 = new Team_Member();
		Team_Member t5 = new Team_Member();
		Team_Member t6 = new Team_Member();
		Team_Member t7 = new Team_Member();
		Team_Member t8 = new Team_Member();
		Team_Member t9 = new Team_Member();	
		
		//comincio a settare i parametri dei team member
		t1.setFName("Luca");
		t1.setLName("Gandolfi");
		t1.setFCode("GNDLCU96D23xxxx");
		t1.setTeam(A);
		t1.setTeamMan(null); //è il pm
		daoTM.create(t1);
		
		t2.setFName("Marco");
		t2.setLName("Rossi");
		t2.setFCode("RSSMRC94D02xxxx");
		t2.setTeam(A);
		t2.setTeamMan(t1);
		daoTM.create(t2);
		
		t3.setFName("Piero");
		t3.setLName("Bianchi");
		t3.setFCode("BNCPRO87D12xxxx");
		t3.setTeam(A);
		t3.setTeamMan(t1);
		daoTM.create(t3);
		
		t4.setFName("Pippo");
		t4.setLName("Inzaghi");
		t4.setFCode("NZGxxxxxxxxx");
		t4.setTeam(B);
		t4.setTeamMan(null); //è il pm
		daoTM.create(t4);
		
		t5.setFName("Rino");
		t5.setLName("Gattuso");
		t5.setFCode("GTSxxxxxxxxx");
		t5.setTeam(B);
		t5.setTeamMan(t4);
		daoTM.create(t5);
			
		t6.setFName("Christian");
		t6.setLName("Abbiati");
		t6.setFCode("BBTxxxxxxxxx");
		t6.setTeam(B);
		t6.setTeamMan(t4);
		daoTM.create(t6);
			
		t7.setFName("Manolo");
		t7.setLName("Rossi");
		t7.setFCode("RSIMNLxxxxxx");
		t7.setTeam(C);
		t7.setTeamMan(null); //è il pm
		daoTM.create(t7);
			
		t8.setFName("Gian Piero");
		t8.setLName("Verdi");
		t8.setFCode("VRDGNP89xxxx");
		t8.setTeam(C);
		t8.setTeamMan(t7);
		daoTM.create(t8);
		
		t9.setFName("Manuel");
		t9.setLName("Minniti");
		t9.setFCode("MNTMNL90xxxx");
		t9.setTeam(C);
		t9.setTeamMan(t7);
		daoTM.create(t9);
		
	}
		
	//funzione da richiamare quando è necessario creare oggetti Client
	public void createClients() {
		JPADao<Long, Client> dao;
		dao = new JPADao<Long, Client>(Client.class);
		
		Client A = new Client();
		Client B = new Client();
		Client C = new Client();
		
		A.setName("Balboa");
		A.setPIVA("xxxxxxxxxxxx");
		dao.create(A);
		
		B.setName("Lindt");
		B.setPIVA("yyyyyyyyyyyy");
		dao.create(B);
		
		C.setName("TeckNet");
		C.setPIVA("zzzzzzzzzzzz");
		dao.create(C);
	}
		
	//funzione da richiamare quando è necessario creare oggetti Project
	public void createProjects() {
		//cerco i team e i client da associare
		JPADao<Long, Team> daoTeam;
		daoTeam = new JPADao<Long, Team>(Team.class);
		Operations<Team> opTeam = new Operations<Team>(Team.class);
		List<Long> nIDs = opTeam.getIDs(); 
		
		long idTeam = nIDs.get(0);
		Team t1 = daoTeam.read(idTeam);
		idTeam++;
		Team t2 = daoTeam.read(idTeam);
		idTeam++;
		Team t3 = daoTeam.read(idTeam);
		//so che sono 3 team per costruzione
			
		JPADao<Long, Client> daoClient;
		daoClient = new JPADao<Long, Client>(Client.class);
		Operations<Client> opClient = new Operations<Client>(Client.class);
		nIDs = opClient.getIDs(); 
		
		long idClient = nIDs.get(0);
		Client c1 = daoClient.read(idClient);
		idClient++;
		Client c2 = daoClient.read(idClient);
			
		//ora posso creare gli oggetti Project e persisterli
		JPADao<Long, Project> daoProject;
		daoProject = new JPADao<Long, Project>(Project.class);
		
		Project A = new Project();
		Project B = new Project();
		Project C = new Project();
		
		final Calendar cal = Calendar.getInstance();
			
		A.setClient(c1);
		A.setName("Project_55");
		A.setTeam(t1);
		cal.add(Calendar.DATE, -360);
		A.setStart(cal.getTime());
		cal.add(Calendar.DATE, +265);
		A.setEnd(cal.getTime());
		daoProject.create(A);
		
		B.setClient(c2);
		B.setName("Project_746");
		B.setTeam(t2);
		cal.add(Calendar.DATE, -103);
		B.setStart(cal.getTime());
		cal.add(Calendar.DATE, +46);
		B.setEnd(cal.getTime());
		daoProject.create(B);
			
		C.setClient(c2);
		C.setName("Project_90");
		C.setTeam(t3);
		cal.add(Calendar.DATE, -10);
		C.setStart(cal.getTime());
		cal.add(Calendar.DATE, +65);
		C.setEnd(cal.getTime());
		daoProject.create(C);
	}
}
