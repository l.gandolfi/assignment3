package test;

import static org.junit.jupiter.api.Assertions.*;

import java.util.List;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import assignment3.JPADao;
import assignment3.Team;
import assignment3.Team_Member;
import services.Operations;

class Team_MemberCRUDTest {
	
	@BeforeEach
	public void beforeMethods() {
		Creates creates = new Creates();
		creates.createTeams();
	}
	
	@AfterEach
	public void afterMethods() {
		Operations<Team> oTeam = new Operations<Team>(Team.class);
		Operations<Team_Member> oTM = new Operations<Team_Member>(Team_Member.class);
		int result;
		result = oTeam.drop();
		result = oTM.drop();
	}

	@Test
	public void testCreate() {
		Creates creates = new Creates();
		Operations<Team_Member> opTeam_Member = new Operations<Team_Member>(Team_Member.class);
		//conto prima della create
		long countb4 = opTeam_Member.count();
		creates.createTeam_Member();
		//conto dopo la create
		long countafter = opTeam_Member.count();
		
		//confronto i due contatori
		assertTrue(countb4 < countafter);
	}
	
	
	@Test
	void testRead() {
		Creates creates = new Creates();
		creates.createTeam_Member();
		Operations<Team_Member> o = new Operations<Team_Member>(Team_Member.class);
		List<Long> ids = o.getIDs();
		
		//leggo l'id del primo team_member
		long id = ids.get(0);
		
		//utilizzo il metodo read per leggere il team_Member
		JPADao<Long, Team_Member> dao;
		dao = new JPADao<Long, Team_Member>(Team_Member.class);
		
		Team_Member t = dao.read(id);
		
		//controllo che la read abbia effettivamente letto l'elemento corretto
		assertEquals(t.getFCode(),"GNDLCU96D23xxxx");

	}
	
	@Test
	void testUpdate() {
		Creates creates = new Creates();
		creates.createTeam_Member();
		
		JPADao<Long, Team_Member> dao;
		dao = new JPADao<Long, Team_Member>(Team_Member.class);
		
		Operations<Team_Member> o = new Operations<Team_Member>(Team_Member.class);
		List<Long> ids = o.getIDs();
		
		//leggo l'id del primo client
		long id = ids.get(0);
		
		//lo leggo e quindi posso modificarlo
		Team_Member t = dao.read(id);
		t.setFName("Temp");
		t = dao.update(t);
		//after the update i can check if the name got updated in the returned entity
		assertEquals(t.getFName(), "Temp");

	}
	
	@Test
	void testDelete() {
		Creates creates = new Creates();
		creates.createTeam_Member();
		
		JPADao<Long, Team_Member> dao;
		dao = new JPADao<Long, Team_Member>(Team_Member.class);
		
		Operations<Team_Member> o = new Operations<Team_Member>(Team_Member.class);
		List<Long> ids = o.getIDs();
		long id = ids.get(2);
		
		long countb4 = o.count();
		//ora leggo il team_member e lo cancello
		Team_Member t = dao.read(id);
		dao.delete(t);
		long countafter = o.count();
		assertTrue(countb4 > countafter);
		
	}

}
