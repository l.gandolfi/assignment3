package test;

import static org.junit.jupiter.api.Assertions.*;

import java.util.List;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;

import assignment3.Client;
import services.ClientService;
import services.Operations;

class ClientServiceTest {

	@AfterEach
	private void afterMethods() {
		Operations<Client> o = new Operations<Client>(Client.class);
		
		int result;
		result = o.drop();
	}
	
	@Test
	void testCreate() {
		//testo quando la tabella è vuota
		ClientService service = new ClientService();
		Operations<Client> o = new Operations<Client>(Client.class);
		long countb4 = o.count();
		service.create("Lindt","yyyyyyyyyyyy");
		long countafter = o.count();
		assertTrue(countb4 < countafter);
		
		//testo quando la tabella non è vuota e name è null
		service.create("zzzzzzzzzzzzz");
		countb4 = countafter;
		countafter = o.count();
		assertTrue(countb4 < countafter);
		
		//testo con partiva iva già esistente
		service.create("zzzzzzzzzzzzz");
		countb4 = countafter;
		countafter = o.count();
		//non deve aver creato
		assertEquals(countb4, countafter);
	}
	
	@Test
	void testRead() {
		//testo con id non presente
		ClientService service = new ClientService();
		Client c = service.read(1);
		//non deve aver letto niente
		assertEquals(c, null);
		
		//testo con id presente
		Creates create = new Creates();
		create.createClients();
		Operations<Client> o = new Operations<Client>(Client.class);
		List<Long> ids = o.getIDs();
		long id = ids.get(0);
		c = service.read(id);
		assertEquals(c.getPIVA(),"xxxxxxxxxxxx");
	}

	@Test
	void testUpdate() {
		//testo quando il client non è presente
		ClientService service = new ClientService();
		Operations<Client> o = new Operations<Client>(Client.class);
		Client c = new Client();
		c.setPIVA("abababababab");
		c = service.update(c);
		List<Long> ids = o.getIDs();
		//non deve aver creato l'oggetto
		assertTrue(ids.size()==0);
		
		//testo quando il client è presente
		Creates create = new Creates();
		create.createClients();
		ids = o.getIDs();
		long id = ids.get(0);
		c = service.read(id);
		c.setName("Temp");
		c = service.update(c);
		assertEquals(c.getName(), "Temp");
	}
	
	@Test
	void testDelete() {
		//testo con oggetto null
		ClientService service = new ClientService();
		Operations<Client> o = new Operations<Client>(Client.class);
		Creates create = new Creates();
		create.createClients();
		Client c = null;
		long countb4 = o.count();
		service.delete(c);
		long countafter = o.count();
		//non deve aver cancellato niente
		assertTrue(countb4 == countafter);
		
		//testo con oggetto non presente
		c = new Client();
		c.setPIVA("abababababa");
		service.delete(c);
		countafter = o.count();
		//non deve aver cancellato niente
		assertTrue(countb4 == countafter);
		
		//testo con oggetto persistente
		List<Long> ids = o.getIDs();
		long id = ids.get(0);
		c = service.read(id);
		service.delete(c);
		countafter = o.count();
		//deve aver cancellato 1 elemento
		assertTrue(countb4 == countafter + 1);
		
		//testo con id non esistente
		service.delete(id); //l'ho già cancellato
		countafter = o.count();
		//non deve aver cancellato niente
		assertTrue(countb4 == countafter + 1);
		
		//testo con id esistente
		id++;
		service.delete(id); 
		countafter = o.count();
		//deve aver cancellato 1 elemento
		assertTrue(countb4 == countafter + 2);
	}
	
	@Test
	void testSearch() {
		//testo la searchByName con name non esistente
		ClientService service = new ClientService();
		Creates create = new Creates();
		create.createClients();
		List<Client> lista = service.searchByName("Mario");
		assertTrue(lista.size()==0);
		
		//testo la searchByName con name esistente
		lista = service.searchByName("TeckNet");
		assertTrue(lista.size()==1);
		
		//testo la searchByPiva con piva non esistente
		Client c = service.searchByPiva("abababababa");
		assertEquals(c, null);
		
		//testo la searchByPiva con piva esistente
		c = service.searchByPiva("xxxxxxxxxxxx");
		assertEquals(c.getName(), "Balboa");
	}
	
	@Test
	void testFindAll() {
		ClientService service = new ClientService();
		Creates create = new Creates();
		create.createClients();
		
		assertTrue(service.findAll().size()>0);
	}
}
