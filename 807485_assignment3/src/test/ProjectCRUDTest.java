package test;

import static org.junit.jupiter.api.Assertions.*;

import java.util.List;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import assignment3.Client;
import assignment3.JPADao;
import assignment3.Project;
import assignment3.Team;
import services.Operations;

class ProjectCRUDTest {
	
	@BeforeEach
	public void beforeMethods() {
		Creates creates = new Creates();
		creates.createTeams();
		creates.createClients();
	}
	
	@AfterEach
	public void afterMethods() {
		Operations<Team> oTeam = new Operations<Team>(Team.class);
		Operations<Client> oClient = new Operations<Client>(Client.class);
		Operations<Project> oP = new Operations<Project>(Project.class);
		int result;
		result = oTeam.drop();
		result = oClient.drop();
		result = oP.drop();
	}

	@Test
	public void testCreate() {
		Creates creates = new Creates();
		Operations<Project> o = new Operations<Project>(Project.class);
		
		//conto prima della create
		long countb4 = o.count();
		creates.createProjects();
		//conto dopo la create
		long countafter = o.count();
		assertTrue(countafter == (countb4 + 3));
		
	}
	
	
	@Test
	void testRead() {
		Creates creates = new Creates();
		creates.createProjects();
		Operations<Project> o = new Operations<Project>(Project.class);
		List<Long> ids = o.getIDs();
		
		//leggo l'id del primo project
		long id = ids.get(0);
		
		//utilizzo il metodo read per leggere il project
		JPADao<Long, Project> dao;
		dao = new JPADao<Long, Project>(Project.class);
		
		Project t = dao.read(id);
		
		//controllo che la read abbia effettivamente letto l'elemento corretto
		assertTrue(t.getName().equals("Project_55"));
		
	}
	
	@Test
	void testUpdate() {
		Creates creates = new Creates();
		creates.createProjects();
		
		Operations<Project> o = new Operations<Project>(Project.class);
		List<Long> ids = o.getIDs();
		
		//leggo l'id del primo project
		long id = ids.get(0);
		
		//utilizzo il metodo read per leggere il project
		JPADao<Long, Project> dao;
		dao = new JPADao<Long, Project>(Project.class);
		Project t = dao.read(id);
		t.setName("Temp");
		t = dao.update(t);
		//after the update i can check if the name got updated in the returned entity
		assertEquals(t.getName(), "Temp");
		
	}
	
	@Test
	void testDelete() {
		Creates creates = new Creates();
		creates.createProjects();
		
		JPADao<Long, Project> dao;
		dao = new JPADao<Long, Project>(Project.class);
		
		Operations<Project> o = new Operations<Project>(Project.class);
		List<Long> ids = o.getIDs();
		long id = ids.get(2);
		
		long countb4 = o.count();
		//ora leggo il client e lo cancello
		Project t = dao.read(id);
		dao.delete(t);
		long countafter = o.count();
		assertTrue(countb4 > countafter);

	}

}
