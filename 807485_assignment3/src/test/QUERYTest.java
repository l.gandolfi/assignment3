package test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import assignment3.Client;
import assignment3.JPADao;
import assignment3.Project;
import assignment3.Team;
import assignment3.Team_Member;
import services.Operations;
import services.QueryExecutor;
import test.Creates;

class QUERYTest {
	
	@BeforeEach
	public void beforeMethods() {
		Creates creates = new Creates();
		creates.createClients();
		creates.createTeams();
		creates.createProjects();
		creates.createTeam_Member();
	}
	
	@AfterEach
	public void afterMethods() {
		Operations<Team> oTeam = new Operations<Team>(Team.class);
		Operations<Client> oClient = new Operations<Client>(Client.class);
		Operations<Project> oProj = new Operations<Project>(Project.class);
		Operations<Team_Member> oTM = new Operations<Team_Member>(Team_Member.class);
		
		int result;
		result = oTeam.drop();
		result = oClient.drop();
		result = oProj.drop();
		result = oTM.drop();
	}

	@Test
	public void testProjectsPerTeam() {
		JPADao<Long, Team> daoTeam;
		daoTeam = new JPADao<Long, Team>(Team.class);
		
		Operations<Team> opTeam = new Operations<Team>(Team.class);
				
		//leggo gli id 
		List<Long> ids = opTeam.getIDs();
		long idTeam = ids.get(0);
		
		/*
		 * 	a questo punto devo testare la corrispondenza di valori
		 *	nella lista auto-generata in Team con la query	
		 */
		String query = "SELECT t FROM assignment3.Project t, assignment3.Team m \n" + 
				"WHERE m.id = "+idTeam+" AND t.team = m";
		
		QueryExecutor qe = new QueryExecutor();
		List<Project> result = qe.executeQuery(query, Project.class);
		
		Team t = daoTeam.read(idTeam);
		
		//le due liste devono avere la stessa dimensione
		assertTrue(t.getProjects().size()==result.size());
		
		//inoltre gli elementi al loro interno devono essere uguali
		for(int i=0; i<result.size(); i++) {
			//utilizzo il metodo equals implementato nell'entità Project
			assertTrue(t.getProjects().get(i).equals(result.get(i)));
		}
	}
	
	@Test
	public void testUpdateClient() {
		//ho intenzione di testare un update effettuando un'update query
		//e di testare quindi il buon esito sfruttando il metodo equals della classe Client
		
		JPADao<Long, Client> daoClient;
		daoClient = new JPADao<Long, Client>(Client.class);
		
		Operations<Client> opClient = new Operations<Client>(Client.class);
		
		//prendo l'id del client da modificare
		List<Long> ids = opClient.getIDs();
		long id = ids.get(0);
		
		//leggo il client prima della modifica
		Client old = daoClient.read(id);
		
		//creo la query
		String query = "UPDATE assignment3.Client c SET c.companyName = \"Bilboa\" WHERE c.id = "+id;
		
		//eseguo
		QueryExecutor qe = new QueryExecutor();
		int result = qe.executeUpdate(query);
		
		//il risultato deve essere diverso da -1 che corrisponde ad un errore
		assertTrue(result != -1);
		
		//controllo che sia andato a buon fine
		Client c = daoClient.read(id);
		assertTrue(c.equals(old));
	}
	
	@Test
	public void testDropClient() {
		/*
		 * eseguo un drop table su client utilizzando i metodi
		 * executeQuery ed executeUpdate
		 */
		Operations<Client> opClient = new Operations<Client>(Client.class);
		
		//creo la query
		String query = "DROP TABLE assignment3.Client";
		
		QueryExecutor qe = new QueryExecutor();
		List<Object> result = qe.executeQuery(query, Object.class);
		
		assertEquals(result, null);
		
		int res = qe.executeUpdate(query);
		
		assertEquals(res,-1);
	}
	
	@Test
	public void testRelationshipTeamandTeam_Member() {
		Operations<Team_Member> op = new Operations<Team_Member>(Team_Member.class);
		
		JPADao<Long, Team_Member> dao;
		dao = new JPADao<Long, Team_Member>(Team_Member.class);
		
		List<Long> ids = op.getIDs();
		long id = ids.get(1);
		
		String query = "SELECT m.project_manager FROM assignment3.Team_Member m WHERE m.id = "+id;
		
		QueryExecutor qe = new QueryExecutor();
		List<Team_Member> result = qe.executeQuery(query, Team_Member.class);
		
		id--;
		//il project manager dovrebbe essere il Team Member con id precedente, secondo costruzione
		Team_Member t = dao.findById(id);
		Team_Member t2 = (Team_Member) result.get(0);
		
		assertTrue(t.equals(t2)); //sfrutto il metodo equals di Team_Member
		
		Operations<Team> opTeam = new Operations<Team>(Team.class);
		ids = opTeam.getIDs();
		id = ids.get(2);
		query = "SELECT COUNT(t.members) FROM assignment3.Team t WHERE t.id = "+id;
		long result2 = qe.executeQuerySingleResult(query, long.class);
		long quantity = 3;
		assertEquals(result2, quantity);
	}

	@Test
	public void testTeam_MemberPerTeam() {
		/*
		 * si intende testare l'attributo getMembers della classe Team
		 */
		Operations<Team> op = new Operations<Team>(Team.class);
		
		JPADao<Long, Team> dao;
		dao = new JPADao<Long, Team>(Team.class);
		
		List<Long> ids = op.getIDs();
		long id = ids.get(0);
		
		//join fatta male ma objectdb da errore su token nelle join
		String query = "SELECT tm FROM assignment3.Team t, assignment3.Team_Member tm "
				+ "WHERE t.id = "+id +" AND tm.team.id = t.id";
		
		QueryExecutor qe = new QueryExecutor();
		List<Team_Member> result = qe.executeQuery(query, Team_Member.class);
		
		Team t = dao.read(id);
		List<Team_Member> members = t.getMembers();
		//attenzione: sono posizionati con ordine inverso
		
		assertEquals(members.size(), result.size());
		
		Team_Member temp1;
		Team_Member temp2;
		//sfruttando il metodo equals di Team_Member
		for(int i=0; i<members.size(); i++) {
			temp1 = result.get(i);
			
			//devo leggerli al contrario
			temp2 = members.get(members.size() - i -1);
			
			assertTrue(temp1.equals(temp2));
		}
	}
	
	@Test
	public void testRecursiveRelation() {
		/*
		 * si intende testare la relazione di project manager tra i Team_Member
		 */
		Operations<Team_Member> op = new Operations<Team_Member>(Team_Member.class);
		
		JPADao<Long, Team_Member> dao;
		dao = new JPADao<Long, Team_Member>(Team_Member.class);
		
		List<Long> ids = op.getIDs();
		/*
		 * prendo il primo id, poichè il primo per costruzione è il project manager stesso
		 */
		long id = ids.get(0);
		String query = "SELECT tm FROM assignment3.Team_Member tm " + 
				"WHERE tm.project_manager.id = "+id;
		
		QueryExecutor qe = new QueryExecutor();
		List<Team_Member> result = qe.executeQuery(query, Team_Member.class);
		
		//leggo il project manager
		Team_Member pManager = dao.read(id);
		
		//istanzio una variabile temp da utilizzare per il test
		Team_Member temp;
		for(int i=1; i<result.size(); i++) {
			id = ids.get(i);
			temp = dao.read(id);
			/*
			 * controllo tramite il metodo getTeamMan e il metodo equals
			 * che il team manager corrisponda al risultato della query
			 */
			assertTrue(temp.getTeamMan().equals(pManager));
			/*
			 * attenzione che nella variabile vengono salvati al contrario
			 * quindi è necessario prendere uno dei due dalla fine
			 */
			assertTrue(temp.equals(result.get(result.size()-i -1)));
			
		}
	}
	
	@Test
	public void testProjectsPerClient() {
		JPADao<Long, Client> daoClient;
		daoClient = new JPADao<Long, Client>(Client.class);
		Operations<Client> oClient = new Operations<Client>(Client.class);
		
		List<Long> ids = oClient.getIDs();
		long id = ids.get(0);
		Client c = daoClient.read(id);
		
		//join fatta male ma objectdb da errore su token nelle join
		String query = "SELECT p FROM assignment3.Client c, assignment3.Project p "
				+ "WHERE c.id = "+id +" AND p.client.id = c.id";
		
		QueryExecutor qe = new QueryExecutor();
		List<Project> result = qe.executeQuery(query, Project.class);
		
		List<Project> projects = c.getProjects();
		//sono in ordine inverso!
		
		//controllo la dimensione
		assertEquals(projects.size(), result.size());
		
		//ora posso ciclare e confrontare oggetto per oggetto
		for(int i=0; i<result.size(); i++) {
			assertTrue(result.get(i).equals(projects.get(result.size() -i -1)));
		}
	}
}
