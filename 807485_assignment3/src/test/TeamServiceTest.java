package test;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Date;
import java.util.List;
import java.util.Calendar;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;

import assignment3.Team;
import services.Operations;
import services.TeamService;

class TeamServiceTest {
	@AfterEach
	private void afterMethods() {
		Operations<Team> oTeam = new Operations<Team>(Team.class);
		
		int result;
		result = oTeam.drop();
	}

	@Test
	void testCreate() {
		final Calendar cal = Calendar.getInstance();
		TeamService service = new TeamService();
		Operations<Team> o = new Operations<Team>(Team.class);
		
		long countb4 = o.count();
		cal.add(Calendar.DATE, -360);
		Date start = cal.getTime();
		cal.add(Calendar.DATE, +100);
		Date end = cal.getTime();
		service.create(990, start, end);
		long countafter = o.count();
		
		assertTrue(countb4 < countafter);
		
		//testo la duplicazione di code 
		//deve gestirmela in automatico
		service.create(990, start, end);
		countb4 = countafter;
		countafter = o.count();
		assertTrue(countb4 < countafter);
		
		//il codice deve essere stato incrementato di 1
		Team t = service.searchByCode(991);
		assertEquals(t.getEnd(),end);
		
		//testo il branch non vuoto con code diverso e senza end
		countb4 = countafter;
		service.create(992, start);
		countafter = o.count();
		assertTrue(countb4 < countafter);
		
		//testo il metodo senza code e senza end
		service.create(start);
		countb4 = countafter;
		countafter = o.count();
		assertTrue(countb4 < countafter);
		
		//svuoto la tabella
		afterMethods();
		
		//testo il branch del primo elemento senza code
		service.create(start, end);
		t = service.searchByCode(001);
		assertEquals(t.getStart(),start);
	}
	
	@Test
	void testRead() {
		//testo quando l'id esiste
		Creates create = new Creates();
		create.createTeams();
		TeamService service = new TeamService();
		Operations<Team> o = new Operations<Team>(Team.class);
		
		List<Long> ids = o.getIDs();
		//leggo l'ultimo id
		long id = ids.get(ids.size()-1);
		Team t = service.read(id);
		assertEquals(t.getCODE(), 992);
		
		//testo quando l'id non esiste
		id++;
		t = service.read(id);
		assertEquals(t, null);
	}
	
	@Test
	void testUpdate() {
		//testo quando il team non è già presente
		Team t = new Team();
		final Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, -360);
		Date start = cal.getTime();
		cal.add(Calendar.DATE, +100);
		Date end = cal.getTime();
		t.setCode(010);
		t.setStart(start);
		t.setEnd(end);
		TeamService service = new TeamService();
		t = service.update(t);
		
		Operations<Team> o = new Operations<Team>(Team.class);
		List<Long> ids = o.getIDs();
		
		//non deve aver creato l'oggetto
		assertTrue(ids.size()==0);
		
		//testo quando il team è già presente
		Creates create = new Creates();
		create.createTeams();
		ids = o.getIDs();
		long id = ids.get(0);
		t = service.read(id);
		t.setCode(001);
		t = service.update(t);
		assertEquals(t.getCODE(),001);
	}
	
	@Test
	void testSearch() {
		//testo il search con tabella vuota
		TeamService service = new TeamService();
		Team t = service.searchByCode(010);
		assertEquals(t,null);
		
		//testo il search con tabella non vuota e code non valido
		Creates create = new Creates();
		create.createTeams();
		t = service.searchByCode(999);
		assertEquals(t,null);
		
		//il caso normale è stato già testato nei metodi precedenti
	}
	
	@Test
	void testDelete() {
		//testo la delete su un elemento null
		TeamService service = new TeamService();
		Creates create = new Creates();
		create.createTeams();
		Operations<Team> o = new Operations<Team>(Team.class);
		long countb4 = o.count();
		Team t = null;
		service.delete(t);
		long countafter = o.count();
		//non deve aver ancellato niente
		assertEquals(countb4, countafter);
		
		//testo la delete su un elemento non null ma non presente nel db
		t = new Team();
		t.setCode(100);
		service.delete(t);
		countafter = o.count();
		//non deve aver cancellato niente
		assertEquals(countb4, countafter);
		
		//testo la delete su un elemento persistente
		List<Long> ids = o.getIDs();
		long id = ids.get(0);
		t = service.read(id);
		service.delete(t);
		countafter = o.count();
		//ne ha cancellato uno
		assertTrue(countb4 == (countafter +1));
		
		//testo la delete con id 
		id++;
		service.delete(id);
		countafter = o.count();
		//ne ha cancellato un altro
		assertTrue(countb4 == (countafter +2));
		
		//ultimo caso: id non esistente
		id--; //ho già cancellato questo elemento
		service.delete(id);
		countafter = o.count();
		//non deve aver cancellato niente
		assertTrue(countb4 == (countafter +2));
	}
	
	@Test
	void testFindAll() {
		TeamService service = new TeamService();
		Creates create = new Creates();
		create.createTeams();
		//deve aver ritornato degli oggetti
		assertTrue(service.findAll().size()>0);
	}

}
