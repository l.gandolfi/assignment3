package test;

import static org.junit.jupiter.api.Assertions.*;

import java.util.List;
import org.junit.jupiter.api.Test;

import assignment3.Client;
import assignment3.JPADao;
import assignment3.Project;
import assignment3.Team;
import services.Operations;

class ClientCRUDTest {

	@Test
	public void testCreate() {
		Creates creates = new Creates();
		Operations<Client> o = new Operations<Client>(Client.class);
		
		//conto prima della create
		long countb4 = o.count();
		creates.createClients();
		//conto dopo la create
		long countafter = o.count();
		assertTrue(countafter == (countb4 + 3));
		
		int result = o.drop();
		assertTrue(result > 0);
	}
	
	
	@Test
	void testRead() {
		Creates creates = new Creates();
		creates.createClients();
		Operations<Client> o = new Operations<Client>(Client.class);
		List<Long> ids = o.getIDs();
		
		//leggo l'id del primo client
		long id = ids.get(0);
		
		//utilizzo il metodo read per leggere il client
		JPADao<Long, Client> dao;
		dao = new JPADao<Long, Client>(Client.class);
		
		Client t = dao.read(id);
		
		//controllo che la read abbia effettivamente letto l'elemento corretto
		assertTrue(t.getPIVA().equals("xxxxxxxxxxxx"));
		
		int result = o.drop();
	}
	
	@Test
	void testUpdate() {
		Creates creates = new Creates();
		creates.createClients();
		
		JPADao<Long, Client> dao;
		dao = new JPADao<Long, Client>(Client.class);
		
		Operations<Client> o = new Operations<Client>(Client.class);
		List<Long> ids = o.getIDs();
		
		//leggo l'id del primo client
		long id = ids.get(0);
		
		//lo leggo e quindi posso modificarlo
		Client t = dao.read(id);
		t.setName("Temp");
		t = dao.update(t);
		//after the update i can check if the name got updated in the returned entity
		assertEquals(t.getName(), "Temp");
		
		int result = o.drop();
	}
	
	@Test
	void testDelete() {
		Creates creates = new Creates();
		creates.createClients();
		
		JPADao<Long, Client> dao;
		dao = new JPADao<Long, Client>(Client.class);
		
		Operations<Client> o = new Operations<Client>(Client.class);
		List<Long> ids = o.getIDs();
		long id = ids.get(2);
		
		long countb4 = o.count();
		//ora leggo il client e lo cancello
		Client t = dao.read(id);
		dao.delete(t);
		long countafter = o.count();
		assertTrue(countb4 > countafter);
		
		int result = o.drop();
	}
	
	@Test
	void testDeleteWithProjects() {
		Creates creates = new Creates();
		creates.createClients();
		creates.createTeams();
		creates.createProjects();
		
		JPADao<Long, Client> dao;
		dao = new JPADao<Long, Client>(Client.class);
		
		Operations<Client> o = new Operations<Client>(Client.class);
		List<Long> ids = o.getIDs();
		//leggo il primo client
		long id = ids.get(0);
		Client c = dao.read(id);
		//prendo la lista di projects
		List<Project> projects = c.getProjects();
		//cancello l'oggetto persistente dal db
		dao.delete(c);
		
		/*
		 * gli elementi presenti nella lista non dovrebbero essere stati cancellati
		 * controllo accedendo ad ogni oggetto
		 */
		JPADao<Long, Project> daoProject;
		daoProject = new JPADao<Long, Project>(Project.class);
		
		for(int i=0; i<projects.size(); i++) {
			Project temp = daoProject.read(projects.get(i).getID());
			assertEquals(temp.getID(),projects.get(i).getID());
		}
		
		//ripulisco il db
		int r = o.drop();
		Operations<Team> oT = new Operations<Team>(Team.class);
		r = oT.drop();
		Operations<Project> oP = new Operations<Project>(Project.class);
		r = oP.drop();
	}
	

}
