package test;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Calendar;
import java.util.List;

import org.junit.jupiter.api.Test;

import assignment3.Client;
import assignment3.JPADao;
import assignment3.Project;
import assignment3.Team;
import assignment3.Team_Member;
import services.Operations;

class TeamCRUDTest {

	@Test
	void testCreate() {		
		Creates creates = new Creates();
		Operations<Team> o = new Operations<Team>(Team.class);
		
		//conto prima della create
		long countb4 = o.count();
		creates.createTeams();
		//conto dopo la create
		long countafter = o.count();
		assertTrue(countafter == (countb4 + 3));
		
		int result = o.drop();
		assertTrue(result > 0);
	}
	
	
	@Test
	void testRead() {
		Creates creates = new Creates();
		creates.createTeams();
		Operations<Team> o = new Operations<Team>(Team.class);
		List<Long> ids = o.getIDs();
		
		//leggo l'id del primo team
		long id = ids.get(0);
		
		//utilizzo il metodo read per leggere il team
		JPADao<Long, Team> dao;
		dao = new JPADao<Long, Team>(Team.class);
		
		Team t = dao.read(id);
		
		//controllo che la read abbia effettivamente letto l'elemento corretto
		assertTrue(t.getCODE()==990);
		
		int result = o.drop();
	}
	
	@Test
	void testUpdate() {
		Creates creates = new Creates();
		creates.createTeams();
		
		JPADao<Long, Team> dao;
		dao = new JPADao<Long, Team>(Team.class);
		
		Operations<Team> o = new Operations<Team>(Team.class);
		List<Long> ids = o.getIDs();
		
		//leggo l'id del primo team
		long id = ids.get(0);
		
		//lo leggo e quindi posso modificarlo
		Team t = dao.read(id);
		
		final Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, +1);
		t.setEnd(cal.getTime());
		t = dao.update(t);
		//after the update i can check if the date got updated in the returned entity
		assertEquals(t.getEnd(), cal.getTime());
		
		int result = o.drop();
	}
	
	@Test
	void testDelete() {
		Creates creates = new Creates();
		creates.createTeams();
		
		JPADao<Long, Team> dao;
		dao = new JPADao<Long, Team>(Team.class);
		
		Operations<Team> o = new Operations<Team>(Team.class);
		List<Long> ids = o.getIDs();
		long id = ids.get(2);
		
		long countb4 = o.count();
		//ora leggo il team e lo cancello
		Team t = dao.read(id);
		dao.delete(t);
		long countafter = o.count();
		assertTrue(countb4 > countafter);
		
		int result = o.drop();
	}
	
	@Test
	public void testDeleteWithDelegation() {
		//questo test serve a verificare la presenza di errori in fase di delete
		Creates creates = new Creates();
		creates.createTeams();
		creates.createTeam_Member();
		creates.createClients(); //mi serve per la create dei progetti
		creates.createProjects();
		
		Operations<Team> opTeam = new Operations<Team>(Team.class);
		List<Long> ids = opTeam.getIDs();

		long id = ids.get(2);
		
		Operations<Project> opProject = new Operations<Project>(Project.class);
		long countb4 = opProject.count(); //dovrà essere uguale anche in seguito
		
		JPADao<Long, Team> daoTeam;
		daoTeam = new JPADao<Long, Team>(Team.class);
		
		Team t = daoTeam.read(id);
		daoTeam.delete(t);
		
		int result = opTeam.drop();
		
		//a questo punto anche i Team_Member associati a questo Team dovrebbero essere cancellati
		//quindi per costruzione i Team Member saranno 9 - 3 = 6
		Operations<Team_Member> opTM = new Operations<Team_Member>(Team_Member.class);
		
		assertTrue(opTM.count()==6);
		
		result = opTM.drop();
		
		//a questo punto è bene controllare che i progetti associati a questo Team siano mantenuti
		//come da accordi, il numero deve essere lo stesso.
		long countafter = opProject.count();
		
		assertEquals(countb4,countafter);
		
		result = opProject.drop();
		
		Operations<Client> opClient = new Operations<Client>(Client.class);
		result = opClient.drop();
	}
	
	@Test
	public void testViolationUnique() {
		Creates creates = new Creates();
		creates.createTeams();
		Throwable exception = assertThrows(javax.persistence.RollbackException.class, () 
				-> { creates.createTeams();	});
		
		Operations<Team> opTeam = new Operations<Team>(Team.class);
		int result = opTeam.drop();
	}
	
}
