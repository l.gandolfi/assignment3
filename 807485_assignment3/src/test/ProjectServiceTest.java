package test;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import assignment3.Client;
import assignment3.Project;
import assignment3.Team;
import services.ClientService;
import services.Operations;
import services.ProjectService;
import services.TeamService;

class ProjectServiceTest {

	@BeforeEach
	public void beforeMethods() {
		Creates creates = new Creates();
		creates.createTeams();
		creates.createClients();
	}
	
	@AfterEach
	public void afterMethods() {
		Operations<Team> oTeam = new Operations<Team>(Team.class);
		Operations<Project> oP = new Operations<Project>(Project.class);
		Operations<Client> oClient = new Operations<Client>(Client.class);
		int result;
		result = oTeam.drop();
		result = oClient.drop();
		result = oP.drop();
	}
	
	@Test
	public void testCreate() {
		//testo la create con tabella vuota e id
		ProjectService service = new ProjectService();
		Operations<Project> oP = new Operations<Project>(Project.class);
		Operations<Team> oTeam = new Operations<Team>(Team.class);
		Operations<Client> oClient = new Operations<Client>(Client.class);
		TeamService sTeam = new TeamService();
		ClientService sClient = new ClientService();
		//trovo un client e un team
		List<Long> ids = oTeam.getIDs();
		long id = ids.get(0);
		ids = oClient.getIDs();
		final Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, -360);
		Date start = cal.getTime();
		cal.add(Calendar.DATE, +100);
		Date end = cal.getTime();
		long countb4 = oP.count();
		service.create("Project_7", start, end, id, ids.get(0));
		long countafter = oP.count();
		assertTrue(countb4 < countafter);
		
		//testo con db non vuoto, senza end e oggetti
		Creates create = new Creates();
		create.createProjects();
		countb4 = countafter;
		service.create("Project_8", start, sTeam.read(id), sClient.read(ids.get(0)));
		countafter = oP.count();
		assertTrue(countb4 < countafter);
		
		//testo con team null
		countb4 = countafter;
		service.create("Project_8", start, null, sClient.read(ids.get(0)));
		countafter = oP.count();
		assertTrue(countb4 == countafter);
		
		//testo con client null
		service.create("Project_8", start, sTeam.read(id), null);
		countafter = oP.count();
		assertTrue(countb4 == countafter);
		
		//testo con id non valido 
		service.create("Project_8", start, --id, ids.get(0));
		countafter = oP.count();
		assertTrue(countb4 == countafter);
		long tempid = ids.get(0);
		service.create("Project_8", start, ++id, --tempid);
		countafter = oP.count();
		assertTrue(countb4 == countafter);
		
	}
	
	@Test
	void testRead() {
		//id esiste
		ProjectService service = new ProjectService();
		Operations<Project> oP = new Operations<Project>(Project.class);
		
		Creates create = new Creates();
		create.createProjects();
		List<Long> ids = oP.getIDs();
		long id = ids.get(0);	
		Project p = service.read(id);
		assertEquals(p.getName(), "Project_55");
	
		//id non esiste
		id--;
		p = service.read(id);
		assertEquals(p, null);
	}
	
	@Test
	void testUpdate() {
		ProjectService service = new ProjectService();
		Operations<Project> oP = new Operations<Project>(Project.class);
		Operations<Team> oTeam = new Operations<Team>(Team.class);
		Operations<Client> oClient = new Operations<Client>(Client.class);
		TeamService sTeam = new TeamService();
		ClientService sClient = new ClientService();
		
		//l'oggetto non è persistente e tabella vuota
		Project p = new Project();
		p.setClient(sClient.read(oClient.getIDs().get(0)));
		p.setTeam(sTeam.read(oTeam.getIDs().get(0)));
		p.setName("Progetto di prova");
		p = service.update(p);
		List<Long> ids = oP.getIDs();
		assertTrue(ids.size()==0);
		
		//l'oggetto è persistente
		Creates create = new Creates();
		create.createProjects();
		ids = oP.getIDs();
		p = service.read(ids.get(0));
		p.setName("Una prova");
		p = service.update(p);
		assertEquals(p.getName(), "Una prova");
	}
	
	@Test
	void testDelete() {
		//testo la delete su un elemento null
		ProjectService service = new ProjectService();
		Creates create = new Creates();
		create.createProjects();
		Operations<Project> o = new Operations<Project>(Project.class);
		long countb4 = o.count();
		Project p = null;
		service.delete(p);
		long countafter = o.count();
		//non deve aver ancellato niente
		assertEquals(countb4, countafter);
		
		//testo la delete su un elemento non null ma non presente nel db
		p = new Project();
		p.setName("provaprovaprova");
		service.delete(p);
		countafter = o.count();
		//non deve aver cancellato niente
		assertEquals(countb4, countafter);
		
		//testo la delete su un elemento persistente
		List<Long> ids = o.getIDs();
		long id = ids.get(0);
		p = service.read(id);
		service.delete(p);
		countafter = o.count();
		//ne ha cancellato uno
		assertTrue(countb4 == (countafter +1));
		
		//testo la delete con id 
		id++;
		service.delete(id);
		countafter = o.count();
		//ne ha cancellato un altro
		assertTrue(countb4 == (countafter +2));
		
		//ultimo caso: id non esistente
		id--; //ho già cancellato questo elemento
		service.delete(id);
		countafter = o.count();
		//non deve aver cancellato niente
		assertTrue(countb4 == (countafter +2));
	}
	
	@Test
	void testFindAll() {
		ProjectService service = new ProjectService();
		Creates create = new Creates();
		create.createProjects();
		//deve aver ritornato degli oggetti
		assertTrue(service.findAll().size()>0);
	}
	
	@Test
	void testSearch() {
		//ho 3 search differenti da testare
		ProjectService service = new ProjectService();
		Creates create = new Creates();
		create.createProjects();
		//search by name con nome non presente
		List<Project> projects = service.searchByName("");
		assertTrue(projects.size()==0);
		//search by name con nome esistente
		projects = service.searchByName("Project_55");
		assertTrue(projects.size()==1);
		
		//search by team con team non presente
		Team t = new Team();
		projects = service.searchByTeam(t);
		assertTrue(projects.size()==0);
		//team esistente
		Operations<Team> oTeam = new Operations<Team>(Team.class);		
		TeamService sTeam = new TeamService();		
		List<Long> ids = oTeam.getIDs();
		projects = service.searchByTeam(sTeam.read(ids.get(0)));
		assertTrue(projects.size()>0);
		
		//search by client con client non presente
		Client c = new Client();
		projects = service.searchByClient(c);
		assertTrue(projects.size()==0);
		//client esistente
		Operations<Client> oClient = new Operations<Client>(Client.class);
		ClientService sClient = new ClientService();
		ids = oClient.getIDs();
		projects = service.searchByClient(sClient.read(ids.get(0)));
		assertTrue(projects.size()>0);
		
	}

}
