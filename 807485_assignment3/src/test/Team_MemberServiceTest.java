package test;

import static org.junit.jupiter.api.Assertions.*;

import java.util.List;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import assignment3.Team;
import assignment3.Team_Member;
import services.Operations;
import services.TeamService;
import services.Team_MemberService;

class Team_MemberServiceTest {

	@BeforeEach
	public void beforeMethods() {
		Creates creates = new Creates();
		creates.createTeams();
	}
	
	@AfterEach
	public void afterMethods() {
		Operations<Team> oTeam = new Operations<Team>(Team.class);
		Operations<Team_Member> oTM = new Operations<Team_Member>(Team_Member.class);
		int result;
		result = oTeam.drop();
		result = oTM.drop();
	}
	
	@Test
	void testCreate() {
		//testo la create con tabella vuota e senza pm
		Team_MemberService service = new Team_MemberService();
		Operations<Team_Member> oTM = new Operations<Team_Member>(Team_Member.class);
		Operations<Team> oTeam = new Operations<Team>(Team.class);
		long countb4 = oTM.count();
		TeamService serviceTeam = new TeamService();
		List<Long> ids = oTeam.getIDs();
		long id = ids.get(0);
		service.create("Piero", "Pelu", "PLPRO45P00D3", serviceTeam.read(id));
		long countafter = oTM.count();
		assertTrue(countb4 < countafter);
		
		//testo la create con un oggetto pm non persistente
		Team_Member pm = new Team_Member();
		pm.setFCode("ssssssssssss");
		pm.setTeam(serviceTeam.read(id));
		countb4 = countafter;
		service.create("Rossi", "rrrr00000s", serviceTeam.read(id) ,pm);
		countafter = oTM.count();
		assertTrue(countb4 < countafter);
		
		//testo con team non persistente: non creo niente
		countb4 = countafter;
		Team temp = new Team();
		temp.setCode(1000);
		service.create("Bianchi", "bbbbb00000s", temp ,pm);
		countafter = oTM.count();
		assertTrue(countb4 == countafter);
		
		//testo la mancanza di nome e pm
		service.create("Verdi", "vvvv00000s", serviceTeam.read(id));
		countafter = oTM.count();
		assertTrue(countb4 < countafter);
		
		//testo con fcode duplicato: non creo niente
		countb4 = countafter;
		service.create("Verdi", "vvvv00000s", serviceTeam.read(id));
		countafter = oTM.count();
		assertTrue(countb4 == countafter);
		
		//testo passando id del team
		service.create("Mario", "Marioli", "mmmmm00000s", id);
		countafter = oTM.count();
		assertTrue(countb4 < countafter);
		
		//testo passando id non esistente: non creo niente
		countb4 = countafter;
		service.create("Mario", "Marioli", "mmmmm00000s", id--);
		countafter = oTM.count();
		assertTrue(countb4 == countafter);
		
		//testo passando id del team e del pm
		id++;
		ids = oTM.getIDs();
		service.create("Vieri", "Bobone", "bbbbbnnnnn", id, ids.get(0));
		countafter = oTM.count();
		assertTrue(countb4 < countafter);
	}
	
	@Test
	void testRead() {
		//testo quando l'id esiste
		Creates creates = new Creates();
		creates.createTeam_Member();
		Team_MemberService service = new Team_MemberService();
		Operations<Team_Member> o = new Operations<Team_Member>(Team_Member.class);
		List<Long> ids = o.getIDs();
		//leggo l'ultimo id
		long id = ids.get(ids.size()-1);
		Team_Member t = service.read(id);
		assertEquals(t.getFCode(), "MNTMNL90xxxx");
		
		//testo quando l'id non esiste
		id++;
		t = service.read(id);
		assertEquals(t, null);
	}
	
	@Test
	void testUpdate() {
		//testo quando non è già presente
		Team_MemberService service = new Team_MemberService();
		TeamService serviceTeam = new TeamService();
		Operations<Team> oTeam = new Operations<Team>(Team.class);
		//leggo gli id dei team
		List<Long> ids = oTeam.getIDs(); 
		Team_Member t = new Team_Member();
		t.setFCode("provaprovaprova");
		t.setTeam(serviceTeam.read(ids.get(0)));
		t = service.update(t);
		
		//ora leggo gli id dei team member
		Operations<Team_Member> o = new Operations<Team_Member>(Team_Member.class);
		ids = o.getIDs();
		
		//non deve aver creato l'oggetto
		assertTrue(ids.size()==0);
			
		//testo quando è già presente
		Creates create = new Creates();
		create.createTeam_Member();
		ids = o.getIDs();
		long id = ids.get(0);
		t = service.read(id);
		t.setFName("Paolo");
		t = service.update(t);
		assertEquals(t.getFName(),"Paolo");
	}
	
	@Test
	void testSearch() {
		//testo il search con tabella vuota
		Team_MemberService service = new Team_MemberService();
		Team_Member t = service.searchByFCode("provaprovaprova");
		assertEquals(t,null);
		List<Team_Member> listTM = service.searchByLastName("Longari");
		assertTrue(listTM.size()==0);
		TeamService serviceTeam = new TeamService();
		Team team = serviceTeam.searchByCode(991);
		listTM = service.searchByTeam(team);
		assertTrue(listTM.size()==0);
		
		//testo il search con tabella non vuota e code non valido
		Creates create = new Creates();
		create.createTeam_Member();
		t = service.searchByFCode("provaprovaprova");
		assertEquals(t,null);
		//nome non valido
		listTM = service.searchByLastName("Longari");
		assertTrue(listTM.size()==0);
		//team non valido
		team = new Team();
		team.setCode(10000);
		listTM = service.searchByTeam(team);
		assertTrue(listTM.size()==0);
		
		//testo quando il codice esiste
		t = service.searchByFCode("NZGxxxxxxxxx");
		assertEquals(t.getFName(),"Pippo");
		//nome esiste
		listTM = service.searchByLastName("Gandolfi");
		assertTrue(listTM.size()>0);
		//team esiste
		team = serviceTeam.searchByCode(991);
		listTM = service.searchByTeam(team);
		assertTrue(listTM.size()>0);
		
	}
	
	@Test
	void testDelete() {
		//testo la delete su un elemento null
		Team_MemberService service = new Team_MemberService();
		Creates create = new Creates();
		create.createTeam_Member();
		Operations<Team_Member> o = new Operations<Team_Member>(Team_Member.class);
		long countb4 = o.count();
		Team_Member t = null;
		service.delete(t);
		long countafter = o.count();
		//non deve aver ancellato niente
		assertEquals(countb4, countafter);
		
		//testo la delete su un elemento non null ma non presente nel db
		t = new Team_Member();
		t.setFCode("provaprovaprova");
		service.delete(t);
		countafter = o.count();
		//non deve aver cancellato niente
		assertEquals(countb4, countafter);
		
		//testo la delete su un elemento persistente
		List<Long> ids = o.getIDs();
		long id = ids.get(0);
		t = service.read(id);
		service.delete(t);
		countafter = o.count();
		//ne ha cancellato uno
		assertTrue(countb4 == (countafter +1));
		
		//testo la delete con id 
		id++;
		service.delete(id);
		countafter = o.count();
		//ne ha cancellato un altro
		assertTrue(countb4 == (countafter +2));
		
		//ultimo caso: id non esistente
		id--; //ho già cancellato questo elemento
		service.delete(id);
		countafter = o.count();
		//non deve aver cancellato niente
		assertTrue(countb4 == (countafter +2));
	}
	
	@Test
	void testFindAll() {
		Team_MemberService service = new Team_MemberService();
		Creates create = new Creates();
		create.createTeam_Member();
		//deve aver ritornato degli oggetti
		assertTrue(service.findAll().size()>0);
	}

}
