package assignment3;

import java.util.*;
import javax.persistence.*;

@Entity(name="TEAM")
@Table(name="Team")
public class Team {

	@Id @GeneratedValue
	@Column(name="T_ID")
	private long id;
	
	@Column(name="CODE", unique=true)
	private long code;
	
	@Column(name="START")
	private Date startT;
	
	@Column(name="END")
	private Date endT;
	
	@Version
    @Column(name = "LAST_UPDATED_TIME")
    private int updatedTime;	
	
	//relazione con i progetti ai quali il team lavora o ha lavorato
	//se venisse cancellao l'oggetto Team, i progetti ad esso associati andranno comunque mantenuti
	@OneToMany(mappedBy="team",targetEntity=Project.class, fetch=FetchType.EAGER, cascade=CascadeType.MERGE)
	private List<Project> projects;
	
	//relazione con i membri del team
	//applico una cascade on delete poichè i membri del team non sono visti come lavoratori dell'azienda ma proprio come 
	//oggetti che appartengono alla classe team e se questa viene a mancare, loro non hanno più modo di esistere
	@OneToMany(mappedBy="team",targetEntity=Team_Member.class, fetch=FetchType.EAGER, cascade=CascadeType.ALL)
	private List<Team_Member> members;
	
	//Setters
	public void setCode(long code) {
		this.code=code;
	}
	
	public void setStart(Date start) {
		startT=start;
	}
	
	public void setEnd(Date end) {
		endT=end;
	}
	
	//Getters
	public long getID() {
		return id;
	}
	
	public long getCODE() {
		return code;
	}
	
	public int getUPT() {
		return updatedTime;
	}
	
	public Date getStart() {
		return startT;
	}
	
	public Date getEnd() {
		return endT;
	}
	
	public List<Team_Member> getMembers() {
		return members;
	}
	
	public List<Project> getProjects(){
		return projects;
	}
	
	public boolean equals(Team t) {
		if(code==t.code && startT.equals(t.startT) && endT.equals(t.endT))
			return true;
		else
			return false;
	}
}
