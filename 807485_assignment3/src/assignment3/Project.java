package assignment3;

import java.util.*;
import javax.persistence.*;

@Entity(name="PROJECT")
@Table(name="Project")
public class Project {

	@Id @GeneratedValue
	@Column(name="P_ID")
	private long id;
	
	@Column(name="P_NAME")
	private String projectName;	
	
	@Column(name="START")
	private Date startP;
	
	@Column(name="END")
	private Date endP;
	
	@Version
    @Column(name = "LAST_UPDATED_TIME")
    private int updatedTime;
	
	@ManyToOne(optional=false, cascade=CascadeType.MERGE) //ogni progetto è associato ad un team che lo esegue
    @JoinColumn(name="T_ID",referencedColumnName="T_ID")
	private Team team;
	
	@ManyToOne(optional=true, cascade=CascadeType.MERGE) //ogni progetto è associato ad un cliente che lo commissiona
    @JoinColumn(name="C_ID",referencedColumnName="C_ID")
	private Client client;
	
	//Setters
	public void setName(String name) {
		projectName=name;
	}
	
	public void setStart(Date start) {
		startP=start;
	}
	
	public void setEnd(Date end) {
		endP=end;
	}
	
	public void setTeam(Team t) {
		team=t;
	}
	
	public void setClient(Client c) {
		client=c;
	}
	
	//Getters
	public long getID() {
		return id;
	}
	
	public String getName() {
		return projectName;
	}
	
	public Date getStart() {
		return startP;
	}
	
	public Date getEnd() {
		return endP;
	}
	
	public int getUPT() {
		return updatedTime;
	}
	
	public Team getTeam() {
		return team;
	}
	
	public Client getClient() {
		return client;
	}
	
	public boolean equals(Project p) {
		if(p.projectName.equals(projectName) && p.startP.equals(startP) && p.endP.equals(endP))
			return true;
		else
			return false;
	}
}
