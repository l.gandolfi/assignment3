package assignment3;

import java.util.List;

public interface Dao<PK, E> {

	//E is the entity type
	//PK is the primary key type
	
	//create new object
	void create(E object);
	
	//read and return existing object
	E read(PK id);
	
	//update existing object and save changes
	E update(E object);
	
	//remove object from the persistent storage
	void delete(E object);
	
	//it's the read
    E findById(PK id);
    
    //return all objects in a list
    List<E> findAll();
   
}
