package assignment3;

import java.util.List;

import javax.persistence.*;

public class JPADao<PK,E> implements Dao<PK,E> {
	
	protected Class<E> entityClass;
	 
	@PersistenceContext
	protected EntityManagerFactory factory;
	protected EntityManager entityManager;
 

  	public JPADao(Class<E> entityClass) {
		this.entityClass = entityClass;
	}
	
 
	@Override
	public E findById(PK id) { 
		connect();
		E t = entityManager.find(entityClass, id); 
		disconnect();
		return t;
	}
	
	@Override
	public List<E> findAll(){
		connect();
		TypedQuery<E> q = entityManager.createQuery("SELECT e FROM "+ this.getType() +" e", entityClass);
		List<E> result = q.getResultList();
		disconnect();
		return result;
	}

	@Override
	public void create(E object) {
		connect();
		entityManager.getTransaction().begin();
		entityManager.persist(object);
		entityManager.getTransaction().commit();
		disconnect();
	}



	@Override
	public E read(PK id) {
		connect();
		E t = entityManager.find(entityClass, id); 
		disconnect();
		return t;
	}



	@Override
	public E update(E object) {
		connect();
		E t = entityManager.merge(object);
		disconnect();
		return t;
	}



	@Override
	public void delete(E object) {
		connect();
		entityManager.getTransaction().begin();
		object = entityManager.merge(object);
		entityManager.remove(object);
		entityManager.getTransaction().commit();
		disconnect();
	}
	
	private void connect() {
		factory = Persistence.createEntityManagerFactory("$objectdb/db/assignment3.odb");
		entityManager = factory.createEntityManager();
	}
	
	private void disconnect() {
		entityManager.close();
		factory.close();
	}
	
	private String getType() {	  
  		//entityClass è nella forma "class assignment3.Team_Member"
  		//devo quindi rimuovere "class " = 6 caratteri da togliere
  		
  		String type = entityClass.toString().substring(6);
  		return type;
  	}
	
}
