package assignment3;

import javax.persistence.*;

@Entity(name="TEAM_MEMBER")
@Table(name="Team_Member")
public class Team_Member {

	@Id @GeneratedValue
	@Column(name="TM_ID")
	private long id;
	
	@Column(name="TM_FNAME")
	private String firstName;	
	
	@Column(name="TM_LNAME")
	private String lastName;
	
	@Column(name="FISCAL_CODE")
	private String f_code;
	
	@Version
    @Column(name = "LAST_UPDATED_TIME")
    private int updatedTime;
	
	@ManyToOne(optional=false, cascade=CascadeType.MERGE) //ogni membro è associato ad un team 
    @JoinColumn(name="T_ID",referencedColumnName="T_ID")
	private Team team;
	
	@ManyToOne(optional=true) //ogni membro è associato ad un Project manager, che è a sua volta membro del team
	//tranne se stesso, che non è associato a nessun manager
	@JoinColumn(name="TM_ID", referencedColumnName="TM_ID")
	private Team_Member project_manager;
	
	//Setters
    public void setTeamMan(Team_Member t) {
    	project_manager=t;
    }
    
    public void setTeam(Team t) {
    	team=t;
    }
    
	public void setFName(String name) {
		firstName=name;
	}
	
	public void setLName(String name) {
		lastName=name;
	}
	
	public void setFCode(String code) {
		f_code=code;
	}
	
	//Getters
	public long getID() {
		return id;
	}
	
	public int getUPT() {
		return updatedTime;
	}
	
	public String getFName() {
		return firstName;
	}
	
	public String getLName() {
		return lastName;
	}
	
	public String getFCode() {
		return f_code;
	}
	
	public Team getTeam() {
		return team;
	}
	
	public Team_Member getTeamMan() {
		return project_manager;
	}

	//f_code identifica in modo univoco, posso utilizzarlo per testare l'uguaglianza
	
	public boolean equals(Team_Member tm) {
		if(this.f_code.equals(tm.f_code))
			return true;
		else
			return false;
	}
	
	
}
