package assignment3;

import java.util.*;
import javax.persistence.*;

@Entity(name="CLIENT")
@Table(name="Client")
public class Client {

	@Id @GeneratedValue
	@Column(name="C_ID")
	private long id;
	
	@Column(name="C_NAME")
	private String companyName;	
	
	@Column(name="P_IVA", unique=true)
	private String p_iva;
	
	@Version
    @Column(name = "LAST_UPDATED_TIME")
    private int updatedTime;
	
	//relazione con tutti i progetti commissionati da un cliente
	//non applico una cascade on delete poichè se no vado a compromettere l'intero sistema
	@OneToMany(mappedBy="client",targetEntity=Project.class, fetch=FetchType.EAGER)
	private List<Project> projects;
	
	//Setters
	public void setName(String name) {
		companyName=name;
	}
	
	public void setPIVA(String piva) {
		p_iva=piva;
	}
	
	//Getters
	public long getID() {
		return id;
	}
	
	public String getName() {
		return companyName;
	}
	
	public String getPIVA() {
		return p_iva;
	}
	
	public int getUPT() {
		return updatedTime;
	}
	
	public List<Project> getProjects() {
		return projects;
	}
	
	public boolean equals(Client c) {
		if(p_iva.equals(c.p_iva))
			return true;
		else
			return false;
	}
}
