package services;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

public class QueryExecutor {
	@PersistenceContext
	protected EntityManagerFactory factory;
	protected EntityManager em;
	
	private void connect() {
		factory = Persistence.createEntityManagerFactory("$objectdb/db/assignment3.odb");
		em = factory.createEntityManager();
	}
	
	private void disconnect() {
		em.close();
		factory.close();
	}

	public <T> List<T> executeQuery(String query, Class<T> type) {
		connect();
		List<T> result;
		
		if(query.contains("DROP") || query.contains("UPDATE") || query.contains("DELETE"))
			result = null;
		else {
			em.getTransaction().begin();
			TypedQuery<T> q = em.createQuery(query, type);
			result = q.getResultList();
			em.getTransaction().commit();
		}
		disconnect();
		return result;
	}
	
	public int executeUpdate(String query) {
		if(!query.contains("DROP")) {
			connect();
			em.getTransaction().begin();
			int result = em.createQuery(query).executeUpdate();
			em.getTransaction().commit();
			disconnect();
			return result;
		}else 
			return -1;
	}
	
	public <T> T executeQuerySingleResult(String query, Class<T> type) {
		T result;
		if(query.contains("DROP") || query.contains("UPDATE") || query.contains("DELETE"))
			result = null;
		else {
			connect();
			em.getTransaction().begin();
			TypedQuery<T> q = em.createQuery(query, type);
			result = q.getSingleResult();
			em.getTransaction().commit();
			disconnect();
		}
		return result;
	}
}
