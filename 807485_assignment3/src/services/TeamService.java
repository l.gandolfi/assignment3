package services;

import java.util.Date;
import java.util.List;
import java.util.Vector;

import assignment3.JPADao;
import assignment3.Team;

public class TeamService {
	
	JPADao<Long, Team> dao;
	
	public void create(long code, Date start, Date end) {
		dao = new JPADao<Long, Team>(Team.class);
		/*
		 * prima di creare l'oggetto e settare il codice devo controllare che sia univoco
		 * per farlo mi salvo tutti i codici e controllo che non sia presente
		 */
		Operations<Team> o = new Operations<Team>(Team.class);
		List<Long> ids = o.getIDs();
		if(ids.size()>0) {
			//entro in questo branch quando non è il primo elemento che creo
			List<Long> codesList = this.setCodes(code, ids);
			
			if(codesList.contains(code))
				this.create(start, end);
			else {
				//posso creare il team
				Team t = new Team();
				t.setCode(code);
				t.setStart(start);
				t.setEnd(end);
				dao.create(t);
			}
		}else {
			//entro in questo branch quando è il primo elemento che creo
			Team t = new Team();
			t.setCode(code);
			t.setStart(start);
			t.setEnd(end);
			dao.create(t);
		}
			
	}
	
	public void create(Date start, Date end) {
		dao = new JPADao<Long, Team>(Team.class);
		Operations<Team> o = new Operations<Team>(Team.class);
		
		/*
		 * manca il codice che deve essere univoco:
		 * mi salvo l'ultimo codice e lo incremento, 
		 */
		List<Long> ids = o.getIDs();
		if(ids.size()>0) {
			//non è il primo elemento quindi posso cercare i codici
			long id = ids.get(ids.size()-1);
			Team temp = dao.read(id);
			long code = temp.getCODE() + 1;
			Team t = new Team();
			t.setCode(code);
			t.setStart(start);
			t.setEnd(end);
			dao.create(t);
		}
		else {
			//è il primo elemento
			Team t = new Team();
			t.setCode(001);
			t.setStart(start);
			t.setEnd(end);
			dao.create(t);
		}
		
	}
	
	public void create(long code, Date start) {
		//il valore di end può essere null
		this.create(code, start, null);
	}
	
	public void create(Date start) {
		//il valore di end può essere a null
		this.create(start, null);
	}
	
	//metodo per ritornare una lista di codici univoci di Team
	private List<Long> setCodes(long code, List<Long> ids) {
		List<Long> codesList = new Vector<Long>();
		Team temp;
		long id;
		for(int i=0; i<ids.size(); i++) {
			id = ids.get(i);
			temp = dao.read(id);
			codesList.add(temp.getCODE());
		}
		return codesList;
	}
	
	public Team read(long id) {
		dao = new JPADao<Long, Team>(Team.class);
		Operations<Team> o = new Operations<Team>(Team.class);
		List<Long> ids = o.getIDs();
		Team t = new Team();
		//controllo se l'id è presente nella lista degli id
		if(ids.contains(id))
			t = dao.read(id);
		else
			t = null;
		return t;
	}
	
	/*
	 * l'update è gestito solamente prendendo in input un oggetto di tipo team 
	 * letto tramite la read e quindi modificato dall'utente
	 * se l'oggetto non era presente nella base dati, non verrà creato
	 */
	public Team update(Team t) {
		dao = new JPADao<Long, Team>(Team.class);
		t = dao.update(t);
		return t;
	}
	
	public void delete(Team t) {
		dao = new JPADao<Long, Team>(Team.class);
		Operations<Team> o = new Operations<Team>(Team.class);
		List<Long> ids = o.getIDs();
		/*
		 * posso cancellarlo soltanto se l'elemento passato non è null
		 * e sia presente nel db quell'oggetto
		 */
		if(t!=null && ids.contains(t.getID()))
			dao.delete(t);
	}
	
	public void delete(long id) {
		dao = new JPADao<Long, Team>(Team.class);
		Operations<Team> o = new Operations<Team>(Team.class);
		List<Long> ids = o.getIDs();
		/*
		 * posso cancellarlo soltanto se esiste nel db un oggetto con quell'id
		 */
		if(ids.contains(id))
			dao.delete(dao.read(id));
	}
	
	public List<Team> findAll() {
		dao = new JPADao<Long, Team>(Team.class);
		return dao.findAll();
	}
	
	public Team searchByCode(long code) {
		dao = new JPADao<Long, Team>(Team.class);
		Operations<Team> o = new Operations<Team>(Team.class);
		Team t;
		List<Long> ids = o.getIDs();
		if(ids.size()>0) {
			//se la tabella non è vuota posso salvarmi i codici
			List<Long> codesList = this.setCodes(code, ids);
			//se il codice esiste
			if(codesList.contains(code)) {
				QueryExecutor qe = new QueryExecutor();
				String query = "SELECT t FROM assignment3.Team t WHERE t.code = "+code;
				t = qe.executeQuerySingleResult(query, Team.class);
			} else 
				t = null;
		} else 
			t = null;
		return t;
	}
}
