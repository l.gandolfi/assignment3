package services;

import java.util.List;

import assignment3.JPADao;
import assignment3.Team;
import assignment3.Team_Member;

public class Team_MemberService {

	JPADao<Long, Team_Member> dao;
	
	/*
	 * nella create è necessario controllare l'esistenza del team passato nel db
	 * e anche l'esistenza del team member.
	 * devo controllare inoltre che f_code sia univoco, altrimenti non creo
	 */
	public void create(String firstName, String lastName, String f_code, Team t, Team_Member pm) {
		Operations<Team_Member> oTM = new Operations<Team_Member>(Team_Member.class);
		dao = new JPADao<Long, Team_Member>(Team_Member.class);
		
		List<Long> ids = oTM.getIDs();
		if(!checkFCode(f_code, ids)) {
			//il codice fiscale è nuovo
			//controllo team
			JPADao<Long, Team> daoTeam;
			daoTeam = new JPADao<Long, Team>(Team.class);
			Team temp = daoTeam.read(t.getID());
			if(temp!=null) {
				//il team esiste
				//controllo il pm
				if(pm==null) {
					Team_Member tm = new Team_Member();
					tm.setFCode(f_code);
					tm.setFName(firstName);
					tm.setLName(lastName);
					tm.setTeam(t);
					tm.setTeamMan(null);
					dao.create(tm);
				}else {
					//se l'oggetto non è null controllo che sia nel db
					Team_Member temp2 = dao.read(pm.getID());
					
					if(temp2==null) {
						//se non è nel db creo l'oggetto team_member
						dao.create(pm);
					}
					//ora è pronto per essere creato
					Team_Member tm = new Team_Member();
					tm.setFCode(f_code);
					tm.setFName(firstName);
					tm.setLName(lastName);
					tm.setTeam(t);
					tm.setTeamMan(pm);
					dao.create(tm);
				}
				
			}
		}
	}
	
	public void create(String firstName, String lastName, String f_code, Team t) {
		this.create(firstName, lastName, f_code, t, null);
	}
	
	public void create(String lastName, String f_code, Team t, Team_Member pm) {
		this.create("", lastName, f_code, t, pm);
	}
	
	public void create(String lastName, String f_code, Team t) {
		this.create("", lastName, f_code, t, null);
	}
	
	/*
	 * posso anche accettare come input l'id di un team al posto del team
	 * e l'id di un project manager al posto dell'oggetto
	 * devo però controllare che esistano gli elementi con quell'id, altrimenti non creo
	 */
	public void create(String firstName, String lastName, String f_code, long idTeam, long idPM) {
		JPADao<Long, Team> daoTeam;
		daoTeam = new JPADao<Long, Team>(Team.class);
		Operations<Team> o = new Operations<Team>(Team.class);
		List<Long> ids = o.getIDs();
		//controllo idTeam
		if(ids.contains(idTeam)) {
			Team t = daoTeam.read(idTeam);
			//ora controllo idPM
			Operations<Team_Member> oTM = new Operations<Team_Member>(Team_Member.class);
			dao = new JPADao<Long, Team_Member>(Team_Member.class);
			ids = oTM.getIDs();
			if(ids.contains(idPM)) {
				Team_Member tm = dao.read(idPM);
				this.create(firstName, lastName, f_code, t, tm);
			}
		}
	}
	
	public void create(String firstName, String lastName, String f_code, long idTeam) {
		JPADao<Long, Team> daoTeam;
		daoTeam = new JPADao<Long, Team>(Team.class);
		Operations<Team> o = new Operations<Team>(Team.class);
		List<Long> ids = o.getIDs();
		if(ids.contains(idTeam)) {
			Team t = daoTeam.read(idTeam);
			this.create(firstName, lastName, f_code, t, null);
		}	
	}
	
	/*
	 * read per id
	 */
	public Team_Member read(long id) {
		dao = new JPADao<Long, Team_Member>(Team_Member.class);
		Operations<Team_Member> o = new Operations<Team_Member>(Team_Member.class);
		List<Long> ids = o.getIDs();
		Team_Member t;
		//controllo se l'id è presente nella lista degli id
		if(ids.contains(id))
			t = dao.read(id);
		else
			t = null;
		return t;
	}
	
	/*
	 * l'update è gestito solamente prendendo in input un oggetto di tipo team_member
	 * letto tramite la read e quindi modificato dall'utente
	 * se l'oggetto non era presente nella base dati, non verrà creato
	 */
	public Team_Member update(Team_Member t) {
		dao = new JPADao<Long, Team_Member>(Team_Member.class);
		t = dao.update(t);
		return t;
	}
	
	/*
	 * delete per oggetto
	 */
	public void delete(Team_Member t) {
		dao = new JPADao<Long, Team_Member>(Team_Member.class);
		Operations<Team_Member> o = new Operations<Team_Member>(Team_Member.class);
		List<Long> ids = o.getIDs();
		/*
		 * posso cancellarlo soltanto se l'elemento passato non è null
		 * e sia presente nel db quell'oggetto
		 */
		if(t!=null && ids.contains(t.getID()))
			dao.delete(t);
	}
	
	/*
	 * delete per id
	 */
	public void delete(long id) {
		dao = new JPADao<Long, Team_Member>(Team_Member.class);
		Operations<Team_Member> o = new Operations<Team_Member>(Team_Member.class);
		List<Long> ids = o.getIDs();
		/*
		 * posso cancellarlo soltanto se esiste nel db un oggetto con quell'id
		 */
		if(ids.contains(id))
			dao.delete(dao.read(id));
	}
	
	/*
	 * findAll per ritornare tutti gli elementi di tipo Team_Member
	 */
	public List<Team_Member> findAll(){
		dao = new JPADao<Long, Team_Member>(Team_Member.class);
		return dao.findAll();
	}
	
	/*
	 * search con differenti politiche:
	 * per cognome, per codice fiscale e per team
	 */
	public List<Team_Member> searchByLastName(String lname){
		QueryExecutor qe = new QueryExecutor();
		String query = "SELECT t FROM assignment3.Team_Member t WHERE t.lastName = \""+lname+"\"";
		return qe.executeQuery(query, Team_Member.class);
	}
	
	public Team_Member searchByFCode(String fcode) {
		//è necessario controllare che fcode esista
		dao = new JPADao<Long, Team_Member>(Team_Member.class);
		Operations<Team_Member> o = new Operations<Team_Member>(Team_Member.class);
		List<Long> ids = o.getIDs();
		Team_Member t = null;
		for(int i=0; i<ids.size(); i++) {
			if(dao.read(ids.get(i)).getFCode().equals(fcode)) {
				//esiste
				QueryExecutor qe = new QueryExecutor();
				String query = "SELECT t FROM assignment3.Team_Member t WHERE t.f_code = \""+fcode+"\"";
				t = qe.executeQuerySingleResult(query, Team_Member.class);
			}
		}
		return t;
	}
	
	public List<Team_Member> searchByTeam(Team team) {		
		long id = team.getID();
		//la query la faccio sull'id
		QueryExecutor qe = new QueryExecutor();
		String query = "SELECT t FROM assignment3.Team_Member t WHERE t.team.id = "+id;
		return qe.executeQuery(query, Team_Member.class);
	}
	
	/*
	 * questo metodo ritorna true se è presente un elemento nel db con quel codice fiscale
	 */
	private boolean checkFCode(String f_code, List<Long> ids) {
		Team_Member temp;
		long id;
		boolean check = false;
		for(int i=0; i<ids.size(); i++) {
			id = ids.get(i);
			temp = dao.read(id);
			if(temp.getFCode().equals(f_code))
				check = true;
		}
		return check;
	}
}
