package services;

import java.util.Date;
import java.util.List;
import java.util.Vector;

import assignment3.Client;
import assignment3.JPADao;

public class ClientService {

	JPADao<Long, Client> dao;
	Operations<Client> o;
	
	public void create(String name, String piva) {
		dao = new JPADao<Long, Client>(Client.class);
		o = new Operations<Client>(Client.class);
		/*
		 * la partiva iva deve essere nuova, se no non creo
		 * controllo inoltre se è il primo elemento
		 */
		List<Long> ids = o.getIDs();
		if(ids.size()>0) {
			//non è vuota
			if(!this.checkPiva(piva, ids)) {
				Client c = new Client();
				c.setName(name);
				c.setPIVA(piva);
				dao.create(c);
			}
				
		} else {
			//è vuota
			Client c = new Client();
			c.setName(name);
			c.setPIVA(piva);
			dao.create(c);
		}
	}
	
	public void create(String piva) {
		this.create(null, piva);
	}
	
	public Client read(long id) {
		dao = new JPADao<Long, Client>(Client.class);
		o = new Operations<Client>(Client.class);
		List<Long> ids = o.getIDs();
		Client c;
		//controllo se l'id è presente nella lista degli id
		if(ids.contains(id))
			c = dao.read(id);
		else
			c = null;
		return c;
	}
	
	/*
	 * l'update è gestito solamente prendendo in input un oggetto di tipo client
	 * letto tramite la read e quindi modificato dall'utente
	 * se l'oggetto non era presente nella base dati, non verrà creato
	 */
	public Client update(Client c) {
		dao = new JPADao<Long, Client>(Client.class);
		c = dao.update(c);
		return c;
	}
	
	public void delete(long id) {
		dao = new JPADao<Long, Client>(Client.class);
		o = new Operations<Client>(Client.class);
		List<Long> ids = o.getIDs();
		/*
		 * posso cancellarlo soltanto se esiste nel db un oggetto con quell'id
		 */
		if(ids.contains(id))
			dao.delete(dao.read(id));
	}
	
	public void delete(Client c) {
		dao = new JPADao<Long, Client>(Client.class);
		o = new Operations<Client>(Client.class);
		List<Long> ids = o.getIDs();
		/*
		 * posso cancellarlo soltanto se l'elemento passato non è null
		 * e sia presente nel db quell'oggetto
		 */
		if(c!=null && ids.contains(c.getID()))
			dao.delete(c);
	}
	
	public List<Client> findAll() {
		dao = new JPADao<Long, Client>(Client.class);
		return dao.findAll();
	}
	
	public List<Client> searchByName(String name) {
		QueryExecutor qe = new QueryExecutor();
		String query = "SELECT t FROM assignment3.Client t WHERE t.companyName = \""+name+"\"";
		return qe.executeQuery(query, Client.class);
	}
	
	public Client searchByPiva(String piva) {
		//controllo che esista la partiva iva
		dao = new JPADao<Long, Client>(Client.class);
		o = new Operations<Client>(Client.class);
		List<Long> ids = o.getIDs();
		Client c = null;
		if(checkPiva(piva, ids)) {	
			QueryExecutor qe = new QueryExecutor();
			String query = "SELECT t FROM assignment3.Client t WHERE t.p_iva = \""+piva+"\"";
			c = qe.executeQuerySingleResult(query, Client.class);
		}
		return c;
	}
	
	private boolean checkPiva(String piva, List<Long> ids) {
		//se la partita iva è presente ritorno true
		Client temp;
		boolean check = false;
		long id;
		for(int i=0; i<ids.size(); i++) {
			id = ids.get(i);
			temp = dao.read(id);
			if(temp.getPIVA().equals(piva))
				check = true;
		}
		return check;
	}
	
}
