package services;

import java.util.Date;
import java.util.List;

import assignment3.Client;
import assignment3.JPADao;
import assignment3.Project;
import assignment3.Team;

public class ProjectService {

	JPADao<Long, Project> dao;
	
	/*
	 * predispongo 4 create diverse, due per oggetti e due per id.
	 * due per data finale e due senza data finale.
	 * in tutti i casi è necessario controllare l'esistenza dei due oggetti nel db
	 */
	public void create(String name, Date start, Date end, Team t, Client c) {
		//controllo che esistano il team e il client nel db
		Operations<Project> o = new Operations<Project>(Project.class);
		dao = new JPADao<Long, Project>(Project.class);
		
		JPADao<Long, Team> daoTeam;
		daoTeam = new JPADao<Long, Team>(Team.class);
		
		//team è null?
		if(t!=null) {
			//non è null -> controllo client
			JPADao<Long, Client> daoClient;
			daoClient = new JPADao<Long, Client>(Client.class);	
			//client è null?
			if(c!=null) {
				//non è null -> creo
				Project p = new Project();
				p.setClient(c);
				p.setTeam(t);
				p.setName(name);
				p.setStart(start);
				p.setEnd(end);
				dao.create(p);
			}
			//è null -> non creo niente
		}
		//se è null non creo niente
	}
	
	public void create(String name, Date start, Team t, Client c) {
		this.create(name, start, null, t, c);
	}
	
	public void create(String name, Date start, Date end, long idTeam, long idClient) {
		//controllo che esistano il team e il client nel db
		dao = new JPADao<Long, Project>(Project.class);
		
		JPADao<Long, Team> daoTeam;
		daoTeam = new JPADao<Long, Team>(Team.class);
		Team temp = daoTeam.read(idTeam);
		//team è null?
		if(temp!=null) {
			//non è null -> controllo client
			JPADao<Long, Client> daoClient;
			daoClient = new JPADao<Long, Client>(Client.class);
			Client ctemp = daoClient.read(idClient);
			//client è null?
			if(ctemp!=null) {
				//non è null -> creo
				Project p = new Project();
				p.setClient(ctemp);
				p.setTeam(temp);
				p.setName(name);
				p.setStart(start);
				p.setEnd(end);
				dao.create(p);
			}
			//è null -> non creo niente
		}
		//se è null non creo niente		
	}
	
	public void create(String name, Date start, long idTeam, long idClient) {
		this.create(name, start, null, idTeam, idClient);
	}
	
	/*
	 * read semplice per id
	 */
	public Project read(long id) {
		Operations<Project> o = new Operations<Project>(Project.class);
		dao = new JPADao<Long, Project>(Project.class);
		List<Long> ids = o.getIDs();
		Project p = null;
		//controllo se l'id è presente nella lista degli id
		if(ids.contains(id))
			p = dao.read(id);
		return p;
	}
	
	/*
	 * l'update è gestito solamente prendendo in input un oggetto di tipo project
	 * letto tramite la read e quindi modificato dall'utente
	 * se l'oggetto non era presente nella base dati, non verrà creato
	 */
	public Project update(Project p) {
		dao = new JPADao<Long, Project>(Project.class);
		p = dao.update(p);
		return p;
	}
	
	/*
	 * la delete è gestita sia per oggetto che per id
	 */
	public void delete(Project p) {
		/*
		 * posso cancellarlo soltanto se l'elemento passato non è null
		 * e sia presente nel db quell'oggetto
		 */
		dao = new JPADao<Long, Project>(Project.class);
		Operations<Project> o = new Operations<Project>(Project.class);
		List<Long> ids = o.getIDs();
		if(p!=null && ids.contains(p.getID()))
			dao.delete(p);
	}
	
	public void delete(long id) {
		dao = new JPADao<Long, Project>(Project.class);
		Operations<Project> o = new Operations<Project>(Project.class);
		List<Long> ids = o.getIDs();
		if(ids.contains(id))
			dao.delete(dao.read(id));
	
	}
	
	/*
	 * findAll per ritornare tutti gli elementi di tipo Project
	 */
	public List<Project> findAll(){
		dao = new JPADao<Long, Project>(Project.class);
		return dao.findAll();
	}
	
	/*
	 * search con differenti politiche:
	 * per nome, per team e per client
	 */
	public List<Project> searchByName(String name){
		QueryExecutor qe = new QueryExecutor();
		String query = "SELECT t FROM assignment3.Project t WHERE t.projectName = \""+name+"\"";
		return qe.executeQuery(query, Project.class);
	}
	
	public List<Project> searchByTeam(Team t) {
		long id = t.getID();
		//la query la faccio sull'id
		QueryExecutor qe = new QueryExecutor();
		String query = "SELECT t FROM assignment3.Project t WHERE t.team.id = "+id;
		return qe.executeQuery(query, Project.class);
	}
	
	public List<Project> searchByClient(Client c) {
		long id = c.getID();
		//la query la faccio sull'id
		QueryExecutor qe = new QueryExecutor();
		String query = "SELECT t FROM assignment3.Project t WHERE t.client.id = "+id;
		return qe.executeQuery(query, Project.class);
	}
}
