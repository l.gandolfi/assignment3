package services;

import java.util.List;

import javax.persistence.*;

public class Operations<E> {

	protected Class<E> entityClass;
	 
	@PersistenceContext
	protected EntityManagerFactory factory;
	protected EntityManager em;

  	public Operations(Class<E> entityClass) {
		this.entityClass = entityClass;
	}
	
  	private String getType() {
  
  		//entityClass è nella forma "class assignment3.Team_Member"
  		//devo quindi rimuovere "class " = 6 caratteri da togliere
  		
  		String type = entityClass.toString().substring(6);
  		return type;
  	}
	
	private void connect() {
		factory = Persistence.createEntityManagerFactory("$objectdb/db/assignment3.odb");
		em = factory.createEntityManager();
	}
	
	private void disconnect() {
		em.close();
		factory.close();
	}
	
	public long count() {
		connect();
		TypedQuery<Long> q= null;
		long result = 0;
		try {
			String query = "SELECT COUNT(t) FROM "+this.getType()+" t";
			q = em.createQuery(query, Long.class);
			result = q.getSingleResult();
		
		}catch(Exception e) {}
		disconnect();
		
		return result;
	}
	
	//metodi per ritornare gli id
	public List<Long> getIDs(){
		connect();
		String query = "SELECT t.id FROM "+this.getType()+" t";
		TypedQuery<Long> q = em.createQuery(query, Long.class);
		List<Long> ids = q.getResultList();

		return ids;
	}
	
	//metodo utile solo in fase di test
	//per una eventuale release ufficiale sarà tolto
	public int drop() {
		connect();
		String query = "DELETE FROM "+this.getType()+" t";
		em.getTransaction().begin();
		int result = em.createQuery(query).executeUpdate();
		em.getTransaction().commit();
		disconnect();
		return result;
	}
}
